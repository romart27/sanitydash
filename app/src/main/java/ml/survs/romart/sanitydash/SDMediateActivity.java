package ml.survs.romart.sanitydash;

/*
  Created by romart on 5/24/17.
  Kali GNU/Linux Rolling 64-bit
*/

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static ml.survs.romart.sanitydash.SDFramework.gameNotify;
import static ml.survs.romart.sanitydash.SDFramework.gamePaused;
import static ml.survs.romart.sanitydash.SDFramework.ih1;
import static ml.survs.romart.sanitydash.SDFramework.ih2;
import static ml.survs.romart.sanitydash.SDFramework.ih3;
import static ml.survs.romart.sanitydash.SDFramework.ih4;
import static ml.survs.romart.sanitydash.SDFramework.isLevelFinished;
import static ml.survs.romart.sanitydash.SDFramework.lifeCount;
import static ml.survs.romart.sanitydash.SDFramework.lifeSpan;
import static ml.survs.romart.sanitydash.SDFramework.onCheck;
import static ml.survs.romart.sanitydash.SDFramework.onHint;
import static ml.survs.romart.sanitydash.SDFramework.onWrong;
import static ml.survs.romart.sanitydash.SDFramework.oncheck;
import static ml.survs.romart.sanitydash.SDFramework.onwrong;
import static ml.survs.romart.sanitydash.SDFramework.problemNumber;
import static ml.survs.romart.sanitydash.SDFramework.scoreUpdate;
import static ml.survs.romart.sanitydash.SDFramework.userScore;

//Predefined Class Imports

public class SDMediateActivity extends AppCompatActivity {

    public static int timerMCounter = 0;
    static TextView scoreValueView;
    static SDMediateActivity sdMediateActivity;
    private SDMediateLibrary questionLibrary = new SDMediateLibrary();
    private TextView questionNumberView;
    private TextView timerM;
    private String userAnswer;
    private String choiceAs;
    private String choiceBs;
    private String choiceCs;
    private String choiceDs;
    private Button choicesA;
    private Button choicesB;
    private Button choicesC;
    private Button choicesD;
    //Class Identity ass a Level
    private int levelIdentity = 2;
    private boolean ver1 = false;

    private SDMediateActivity.MyMediateTimer myMediateTimer;

    private ProgressBar progressBar;
    //Initialization of OnTick Sound
    private MediaPlayer ontick;
    private String Hint = "";
    //Enable Context Passing to other activity
    private Context ctx = SDMediateActivity.this;

    //Instantiation of the activity
    public static SDMediateActivity getInstance() {
        return sdMediateActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_two);
        frameworkInitializer();

        sdMediateActivity = this;

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        scoreValueView = (TextView) findViewById(R.id.scoreValue);
        ontick = MediaPlayer.create(ctx, R.raw.alarming);
        questionNumberView = (TextView) findViewById(R.id.questionNumber);
        timerM = (TextView) findViewById(R.id.timerM);

        Button favHint = (Button) findViewById(R.id.favHint);

        choicesA = (Button) findViewById(R.id.choiceA);

        favHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHint(ctx, Hint);
            }
        });

        choicesA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceAs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesB = (Button) findViewById(R.id.choiceB);
        choicesB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceBs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesC = (Button) findViewById(R.id.choiceC);
        choicesC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceCs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesD = (Button) findViewById(R.id.choiceD);
        choicesD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceDs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });
        lifeSpan(lifeCount, ctx, levelIdentity);
        refreshProblem();
    }

    public void frameworkInitializer() {
        ih1 = (ImageView) findViewById(R.id.ih1);
        ih2 = (ImageView) findViewById(R.id.ih2);
        ih3 = (ImageView) findViewById(R.id.ih3);
        ih4 = (ImageView) findViewById(R.id.ih4);
        onwrong = MediaPlayer.create(ctx, R.raw.onwrong);
        oncheck = MediaPlayer.create(ctx, R.raw.oncheck);
    }

    private void refreshProblem() {
        if (lifeCount < 1) {
            myMediateTimer.cancel();
        } else {
            if (ver1) {
                myMediateTimer.cancel();
            } else {
                ver1 = true;
            }
            timerStart(true, 0);
        }
        if (problemNumber < 10) {
            NumberFormat valueDecimal = new DecimalFormat("0");
            int[][] cTemp = {
                    {1, 2, 3, 0},
                    {2, 1, 3, 0},
                    {3, 2, 0, 1},
                    {1, 0, 3, 2}
            };
            //Randomize Problem Choices
            int x = Integer.parseInt(valueDecimal.format(Math.floor(Math.random() * 4)));
            //Randomize Problem from 0 to 29
            int temp = Integer.parseInt(valueDecimal.format(Math.floor(Math.random() * 19)));
            //Setting up for problem Question Image
            TextView problemWrapper = (TextView) findViewById(R.id.problemWrapper);

            problemWrapper.setText(questionLibrary.getProblem(temp));
            choicesA.setText(questionLibrary.choiceA(temp, cTemp[x][0]));
            choicesB.setText(questionLibrary.choiceA(temp, cTemp[x][1]));
            choicesC.setText(questionLibrary.choiceA(temp, cTemp[x][2]));
            choicesD.setText(questionLibrary.choiceA(temp, cTemp[x][3]));
            userAnswer = questionLibrary.getAnswers(temp);
            Hint = questionLibrary.getHint(temp);
            choiceAs = questionLibrary.choiceA(temp, cTemp[x][0]);
            choiceBs = questionLibrary.choiceB(temp, cTemp[x][1]);
            choiceCs = questionLibrary.choiceC(temp, cTemp[x][2]);
            choiceDs = questionLibrary.choiceD(temp, cTemp[x][3]);
            problemNumber++;
            questionNumberView.setText(getString(R.string.lvl_one_15_counter) + String.valueOf(problemNumber));
        } else {
            myMediateTimer.cancel();
            Intent LevelFinal = new Intent(getApplicationContext(), SDFinalActivity.class);
            if (userScore == 2250) {
                userScore = +2000;
                lifeCount = +1;
                startActivity(LevelFinal);
                isLevelFinished(levelIdentity, ctx);
            } else if (userScore <= 2249 && userScore >= 2000) {
                startActivity(LevelFinal);
                isLevelFinished(levelIdentity, ctx);
            } else {
                myMediateTimer.cancel();
                gameNotify(ctx, levelIdentity);
                myMediateTimer.cancel();
            }

        }
    }

    public void timerStart(boolean ver2, int modValue) {
        if (ver2) {
            myMediateTimer = new SDMediateActivity.MyMediateTimer(61000, 500);
            myMediateTimer.start();
        } else {
            myMediateTimer = new SDMediateActivity.MyMediateTimer(modValue * 1000, 500);
            myMediateTimer.start();
        }
    }

    @Override
    public void onBackPressed() {
        myMediateTimer.cancel();
        gamePaused(ctx, levelIdentity);
    }

    public void quit() {
        super.onBackPressed();
    }

    public void resume() {
        myMediateTimer = new SDMediateActivity.MyMediateTimer((timerMCounter * 1000), 500);
        myMediateTimer.start();
    }

    private class MyMediateTimer extends CountDownTimer {

        MyMediateTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            timerMCounter = Integer.parseInt(String.valueOf(millisUntilFinished / 1000));
            timerM.setText(String.valueOf(timerMCounter) + "s");
            progressBar.setProgress(timerMCounter);
            if (timerMCounter <= 10) {
                ontick.start();
            }
        }

        @Override
        public void onFinish() {
            refreshProblem();
            onWrong(ctx, false, levelIdentity);
            myMediateTimer.cancel();
            Toast.makeText(SDMediateActivity.this, "Times Up", Toast.LENGTH_SHORT).show();
            if (lifeCount < 1) {
                myMediateTimer.cancel();
            } else {
                timerStart(true, 0);
            }
        }

    }
}


