package ml.survs.romart.sanitydash;

/*
  Created by romart on 5/21/17.
  Kali GNU/Linux Rolling 64-bit
 */

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

public class SDMainActivity extends AppCompatActivity {

    boolean confirmValue = false;
    private boolean levelOne = false;
    private boolean levelTwo = false;
    private boolean levelFinal = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sdmain);

        //Retrieving of old Game History
        onDataRetrieve();

        //Initialization of Favicon's having an icon of play/lock signs
        FloatingActionButton favOne = (FloatingActionButton) findViewById(R.id.favLevelOne);
        FloatingActionButton favTwo = (FloatingActionButton) findViewById(R.id.favLevelTwo);
        FloatingActionButton favFinal = (FloatingActionButton) findViewById(R.id.favLevelFinal);

        //Initialization of Level Image Button Easy|Mediate|Final
        ImageButton ifavOne = (ImageButton) findViewById(R.id.ifavLevelOne);
        ImageButton ifavTwo = (ImageButton) findViewById(R.id.ifavLevelTwo);
        ImageButton ifavFinal = (ImageButton) findViewById(R.id.ifavLevelFinal);
        FloatingActionButton favMenu = (FloatingActionButton) findViewById(R.id.floatingActionMenu);

        //Favicon's Onclick Actions
        favMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onGameMenu();
            }
        });
        favOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLevelOne();
            }
        });
        favTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLevelTwo();
            }
        });
        favFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLevelFinal();
            }
        });

        //ImageButton OnClick Actions
        ifavOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLevelOne();
            }
        });
        ifavTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLevelTwo();
            }
        });
        ifavFinal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onLevelFinal();
            }
        });

    }

    @Override
    public void onBackPressed() {
        //When User tap the (Hardware/Software BACK Button) this function Executed,
        // whether to (EXIT) the app or (CANCEL).
        if (confirmValue) {
            System.exit(1);
        } else {
            //Showing of the Confirmation Dialog
            AlertDialog.Builder exit_alert = new AlertDialog.Builder(this);
            exit_alert.setMessage("Are you sure,You wanted to Exit the Game?");
            exit_alert.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    confirmValue = true;
                    onBackPressed();
                }
            });

            exit_alert.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            AlertDialog alertDialog = exit_alert.create();
            alertDialog.show();
        }
    }

    private void onLevelOne() {
        //When ImageButton of (Easy Level) tapped this line Executed
        if (levelOne) {
            startActivity(new Intent(getApplicationContext(), SDEasyActivity.class));
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Level Locked!");
            alert.setIcon(R.drawable.ic_lock);
            alert.setMessage("Try to play with the Developer first");
            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        }
    }

    private void onLevelTwo() {
        //When ImageButton of (Mediate Level) tapped this line Executed
        if (levelTwo) {
            startActivity(new Intent(getApplicationContext(), SDMediateActivity.class));
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Level Locked!");
            alert.setIcon(R.drawable.ic_lock);
            alert.setMessage("Try to play the Easy level first");
            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        }
    }

    private void onLevelFinal() {
        //When ImageButton of (Final Level) tapped this line Executed
        if (levelFinal) {
            startActivity(new Intent(getApplicationContext(), SDFinalActivity.class));
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(this);
            alert.setTitle("Level Locked!");
            alert.setIcon(R.drawable.ic_lock);
            alert.setMessage("Try to play the Mediate level first");
            AlertDialog alertDialog = alert.create();
            alertDialog.show();
        }
    }

    private void onGameMenu() {
        //When the floating Action Button located at the bottom right side was tapped
        //this function executed.
        AlertDialog.Builder game_menu = new AlertDialog.Builder(this);
        LayoutInflater gameMenu_inflater = getLayoutInflater();
        View overView = gameMenu_inflater.inflate(R.layout.layout_menu, null);
        game_menu.setView(overView);
        Button scoreBoard = (Button) overView.findViewById(R.id.score_board);
        Button help = (Button) overView.findViewById(R.id.help);
        Button aboutApp = (Button) overView.findViewById(R.id.about_app);
        final AlertDialog gameMenu_inflater_Dialog = game_menu.create();

        scoreBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SDScoreActivity.class));
                gameMenu_inflater_Dialog.dismiss();
            }
        });

        help.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(), SDHelpActivity.class));
                gameMenu_inflater_Dialog.dismiss();
            }
        });
        aboutApp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onAboutApp();
                gameMenu_inflater_Dialog.dismiss();
            }
        });
        gameMenu_inflater_Dialog.show();
    }

    private void onAboutApp() {
        AlertDialog.Builder game_about = new AlertDialog.Builder(this);
        LayoutInflater gameAbout_inflater = getLayoutInflater();
        View overView = gameAbout_inflater.inflate(R.layout.activity_about, null);
        game_about.setView(overView);
        final AlertDialog gameAbout_inflater_Dialog = game_about.create();
        gameAbout_inflater_Dialog.show();
    }

    private void onDataRetrieve() {
        //When the startup of the App this function is Executed for
        //Data retrieving and validating if the user has already surpassed the higher levels, if not
        //Boolean value for respected level variable will return false which returns (Level Lock).
        FloatingActionButton l1 = (FloatingActionButton) findViewById(R.id.favLevelOne);
        FloatingActionButton l2 = (FloatingActionButton) findViewById(R.id.favLevelTwo);
        FloatingActionButton l3 = (FloatingActionButton) findViewById(R.id.favLevelFinal);


        //Data History Query
        levelOne = true;
        levelTwo = false;
        levelFinal = false;

        l1.setImageResource(R.drawable.ic_play_arrow);

        if (levelTwo) {
            l2.setImageResource(R.drawable.ic_play_arrow);
        }
        if (levelFinal) {
            l3.setImageResource(R.drawable.ic_play_arrow);
        }
    }
}

