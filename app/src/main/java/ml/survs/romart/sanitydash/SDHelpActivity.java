package ml.survs.romart.sanitydash;

/**
 * Created by romart on 5/21/17.
 * Kali GNU/Linux Rolling 64-bit
 */

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class SDHelpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);

        FloatingActionButton favBack = (FloatingActionButton) findViewById(R.id.favBack);
        favBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SDHelpActivity.super.finish();
                SDHelpActivity.super.onBackPressed();
            }
        });
    }
}
