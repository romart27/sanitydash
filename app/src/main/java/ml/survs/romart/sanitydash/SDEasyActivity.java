package ml.survs.romart.sanitydash;

/*
  Created by romart on 5/21/17.
  Kali GNU/Linux Rolling 64-bit
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static ml.survs.romart.sanitydash.SDFramework.gameNotify;
import static ml.survs.romart.sanitydash.SDFramework.gamePaused;
import static ml.survs.romart.sanitydash.SDFramework.ih1;
import static ml.survs.romart.sanitydash.SDFramework.ih2;
import static ml.survs.romart.sanitydash.SDFramework.ih3;
import static ml.survs.romart.sanitydash.SDFramework.ih4;
import static ml.survs.romart.sanitydash.SDFramework.isLevelFinished;
import static ml.survs.romart.sanitydash.SDFramework.lifeCount;
import static ml.survs.romart.sanitydash.SDFramework.lifeSpan;
import static ml.survs.romart.sanitydash.SDFramework.onCheck;
import static ml.survs.romart.sanitydash.SDFramework.onHint;
import static ml.survs.romart.sanitydash.SDFramework.onWrong;
import static ml.survs.romart.sanitydash.SDFramework.oncheck;
import static ml.survs.romart.sanitydash.SDFramework.onwrong;
import static ml.survs.romart.sanitydash.SDFramework.problemNumber;
import static ml.survs.romart.sanitydash.SDFramework.scoreUpdate;
import static ml.survs.romart.sanitydash.SDFramework.userScore;


public class SDEasyActivity extends AppCompatActivity {

    public static TextView scoreValueView;
    public static int timerECounter = 0;
    public static TextView checkView;
    public static TextView wrongView;
    static SDEasyActivity sdEasyActivity;
    private SDEasyLibrary questionLibrary = new SDEasyLibrary();
    private TextView questionNumberView;
    private TextView timerE;
    private Object userAnswer;
    private Object choiceA;
    private Object choiceB;
    private Object choiceC;
    private Object choiceD;
    private ImageButton choicesA;
    private ImageButton choicesB;
    private ImageButton choicesC;
    private ImageButton choicesD;
    private int levelIdentity = 1;

    private MediaPlayer ontick;

    private boolean ver1 = false;
    private MyEasyTimer myEasyTimer;
    private ProgressBar progressBar;
    private String Hint = "";
    private Context ctx = SDEasyActivity.this;

    public static SDEasyActivity getInstance() {
        return sdEasyActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_one);
        frameworkInitializer();
        sdEasyActivity = this;
        SharedPreferences dataRetrieve = PreferenceManager.getDefaultSharedPreferences(ctx);
        if (Boolean.parseBoolean(dataRetrieve.getString("resumeOne", "false"))) {
            //Showing of the Confirmation Dialog
            AlertDialog.Builder retrieve = new AlertDialog.Builder(this);
            retrieve.setMessage("Do you want to resume the past game?");
            retrieve.setPositiveButton("New ", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface arg0, int arg1) {
                    SDEasyActivity.getInstance().recreate();
                    SharedPreferences gss = PreferenceManager.getDefaultSharedPreferences(ctx);
                    SharedPreferences.Editor edit = gss.edit();
                    edit.putString("resumeOne", String.valueOf(false));
                    edit.apply();
                }
            });

            retrieve.setNegativeButton("Resume", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    SharedPreferences gss = PreferenceManager.getDefaultSharedPreferences(ctx);
                    SharedPreferences.Editor edit = gss.edit();
                    edit.putString("resumeOne", String.valueOf(false));
                    edit.putString("onRunOne", String.valueOf(true));
                    edit.apply();
                    SDEasyActivity.getInstance().recreate();
                }
            });
            AlertDialog alertDialog = retrieve.create();
            alertDialog.show();
        } else {
            SDEasyActivity.getInstance().startGame();
        }

    }

    public void startGame() {
        SharedPreferences dataRetrieve = PreferenceManager.getDefaultSharedPreferences(ctx);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        ontick = MediaPlayer.create(ctx, R.raw.alarming);
        scoreValueView = (TextView) findViewById(R.id.scoreValue);
        questionNumberView = (TextView) findViewById(R.id.questionNumber);
        timerE = (TextView) findViewById(R.id.timerE);

        checkView = (TextView) findViewById(R.id.checkView);
        wrongView = (TextView) findViewById(R.id.wrongView);

        FloatingActionButton favHint = (FloatingActionButton) findViewById(R.id.favHint);

        choicesA = (ImageButton) findViewById(R.id.choiceOne);

        favHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHint(ctx, Hint);
            }
        });

        choicesA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceA)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesB = (ImageButton) findViewById(R.id.choiceTwo);
        choicesB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceB)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesC = (ImageButton) findViewById(R.id.choiceThree);
        choicesC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceC)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesD = (ImageButton) findViewById(R.id.choiceFour);
        choicesD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceD)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        if (Boolean.parseBoolean(dataRetrieve.getString("onRunOne", "false"))) {
            lifeCount = Integer.parseInt(dataRetrieve.getString("lifeCountOne", "4"));
        } else {
            lifeCount = 4;
        }
        lifeSpan(lifeCount, ctx, levelIdentity);
        refreshProblem();
    }

    @Override
    public void onBackPressed() {
        myEasyTimer.cancel();
        gamePaused(ctx, levelIdentity);
    }

    public void frameworkInitializer() {
        ih1 = (ImageView) findViewById(R.id.ih1);
        ih2 = (ImageView) findViewById(R.id.ih2);
        ih3 = (ImageView) findViewById(R.id.ih3);
        ih4 = (ImageView) findViewById(R.id.ih4);
        onwrong = MediaPlayer.create(ctx, R.raw.onwrong);
        oncheck = MediaPlayer.create(ctx, R.raw.oncheck);
    }

    private void refreshProblem() {
        SharedPreferences dataRetrieve = PreferenceManager.getDefaultSharedPreferences(ctx);
        if (lifeCount < 1) {
            myEasyTimer.cancel();
        } else {
            if (ver1) {
                myEasyTimer.cancel();
            } else {
                ver1 = true;
            }
            if (Boolean.parseBoolean(dataRetrieve.getString("onRunOne", "false"))) {
                timerStart(false, Integer.parseInt(dataRetrieve.getString("timerOne", "20")));
                scoreUpdate(Integer.parseInt(dataRetrieve.getString("userScoreKeyOne", "20")), levelIdentity);
            } else {
                timerStart(true, 0);
            }
        }
        if (problemNumber < 15) {
            NumberFormat valueDecimal = new DecimalFormat("0");
            int[][] cTemp = {
                    {1, 2, 3, 0},
                    {2, 1, 3, 0},
                    {3, 2, 0, 1},
                    {1, 0, 3, 2}
            };
            //Randomize Problem Choices
            int x = Integer.parseInt(valueDecimal.format(Math.floor(Math.random() * 4)));
            //Randomize Problem from 0 to 29
            int temp = Integer.parseInt(valueDecimal.format(Math.floor(Math.random() * 29)));
            //Setting up for problem Question Image
            ImageView imageContent = (ImageView) findViewById(R.id.imageContent);
            imageContent.setImageResource((Integer) questionLibrary.getProblem(temp));

            choicesA.setImageResource((Integer) questionLibrary.choiceA(temp, cTemp[x][0]));
            choicesB.setImageResource((Integer) questionLibrary.choiceB(temp, cTemp[x][1]));
            choicesC.setImageResource((Integer) questionLibrary.choiceC(temp, cTemp[x][2]));
            choicesD.setImageResource((Integer) questionLibrary.choiceD(temp, cTemp[x][3]));

            userAnswer = questionLibrary.getAnswer(temp);
            Hint = questionLibrary.getHint(temp);
            choiceA = questionLibrary.choiceA(temp, cTemp[x][0]);
            choiceB = questionLibrary.choiceB(temp, cTemp[x][1]);
            choiceC = questionLibrary.choiceC(temp, cTemp[x][2]);
            choiceD = questionLibrary.choiceD(temp, cTemp[x][3]);
            problemNumber++;
            if (Boolean.parseBoolean(dataRetrieve.getString("onRunOne", "false"))) {
                SharedPreferences gss = PreferenceManager.getDefaultSharedPreferences(ctx);
                SharedPreferences.Editor edit = gss.edit();
                problemNumber = Integer.parseInt(dataRetrieve.getString("problemNumberOne", "1"));
                edit.putString("onRunOne", String.valueOf(false));
                edit.apply();
            }
            wrongView.setText(String.valueOf(4 - lifeCount));
            questionNumberView.setText(getString(R.string.lvl_one_15_counter) + String.valueOf(problemNumber));
        } else {
            myEasyTimer.cancel();
            Intent LevelTwo = new Intent(getApplicationContext(), SDMediateActivity.class);
            if (userScore == 2250) {
                userScore = +1000;
                lifeCount = +1;
                startActivity(LevelTwo);
                isLevelFinished(levelIdentity, ctx);
            } else if (userScore <= 2249 && userScore >= 2000) {
                startActivity(LevelTwo);
                isLevelFinished(levelIdentity, ctx);
            } else {
                myEasyTimer.cancel();
                gameNotify(ctx, levelIdentity);
                myEasyTimer.cancel();
            }

        }
    }

    private void timerStart(boolean ver2, int modValue) {
        if (ver2) {
            myEasyTimer = new MyEasyTimer(21000, 500);
            myEasyTimer.start();
        } else {
            myEasyTimer = new MyEasyTimer(modValue * 1000, 500);
            myEasyTimer.start();
        }
    }

    public void quit() {
        super.onBackPressed();
        SharedPreferences gss = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor edit = gss.edit();
        edit.putString("resumeOne", String.valueOf(true));
        edit.putString("userScoreKeyOne", String.valueOf(userScore));
        edit.putString("lifeCountOne", String.valueOf(lifeCount));
        edit.putString("problemNumberOne", String.valueOf(problemNumber));
        edit.putString("timerOne", String.valueOf(timerECounter));
        edit.apply();
    }

    public void resume() {
        myEasyTimer = new MyEasyTimer((timerECounter * 1000), 500);
        myEasyTimer.start();
    }

    private class MyEasyTimer extends CountDownTimer {

        MyEasyTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            timerECounter = Integer.parseInt(String.valueOf(millisUntilFinished / 1000));
            timerE.setText(String.valueOf(timerECounter) + "s");
            progressBar.setProgress(timerECounter);
            if (timerECounter <= 5) {
                ontick.start();
            }
        }

        @Override
        public void onFinish() {
            refreshProblem();
            onWrong(ctx, false, levelIdentity);
            myEasyTimer.cancel();
            Toast.makeText(SDEasyActivity.this, "Times Up", Toast.LENGTH_SHORT).show();
            if (lifeCount < 1) {
                myEasyTimer.cancel();
            } else {
                timerStart(true, 0);
            }
        }

    }
}
