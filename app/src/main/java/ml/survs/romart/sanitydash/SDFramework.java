package ml.survs.romart.sanitydash;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Timer;
import java.util.TimerTask;

import static ml.survs.romart.sanitydash.SDEasyActivity.checkView;
import static ml.survs.romart.sanitydash.SDEasyActivity.timerECounter;
import static ml.survs.romart.sanitydash.SDFinalActivity.timerFCounter;
import static ml.survs.romart.sanitydash.SDMediateActivity.timerMCounter;

class SDFramework {

    static int problemNumber = 0;
    static int userScore = 0;
    static int lifeCount = 4;
    static int userChecked = 0;

    static ImageView ih1;
    static ImageView ih2;
    static ImageView ih3;
    static ImageView ih4;

    static MediaPlayer onwrong;
    static MediaPlayer oncheck;

    //String used for saving basic information on Resume Implementation
    static String[][] gameState = {
            {"EasyLevelKey", "EasyScoreKey", "EasyLifeKey", "EasyProblemNumKey", "EasyTimeKey"},
            {"MediateLevelKey", "MediateScoreKey", "MediateLifeKey", "MediateProblemNumKey", "MediateTimeKey"},
            {"FinalLevelKey", "FinalScoreKey", "FinalLifeKey", "FinalProblemNumKey", "FinalTimeKey"}
    };

    static int[] gameTimeState = {
            timerECounter,
            timerMCounter,
            timerFCounter
    };

    static String[] levelKeys = {
            "LevelEasyKey",
            "LevelMediateKey",
            "LevelFinalKey"
    };


    static void onHint(Context ctx, String Hint) {
        AlertDialog.Builder alert = new AlertDialog.Builder(ctx);
        alert.setTitle("Problem Hint");
        alert.setMessage(Hint);
        AlertDialog alertDialog = alert.create();
        alertDialog.show();
    }

    static void onCheck(Context ctx) {
        //Toast
        Toast.makeText(ctx, "Correct", Toast.LENGTH_SHORT).show();
        //Audio
        oncheck.start();
    }

    static void onWrong(Context ctx, boolean toast, int level) {
        if (toast) {
            //Toast
            Toast.makeText(ctx, "Wrong", Toast.LENGTH_SHORT).show();
        }
        //Audio
        onwrong.start();
        //Score Update
        lifeCount = lifeCount - 1;
        lifeSpan(lifeCount, ctx, level);
    }

    static void lifeSpan(int lifespan, Context ctx, int level) {

        if (lifespan < 1) {
            ih1.setImageResource(R.drawable.ic_heart_fillout);
            ih2.setImageResource(R.drawable.ic_heart_fillout);
            ih3.setImageResource(R.drawable.ic_heart_fillout);
            ih4.setImageResource(R.drawable.ic_heart_fillout);
            gameOver(ctx, level);
        }
        if (lifespan == 1) {
            ih1.setImageResource(R.drawable.ic_heart_fill);
            ih2.setImageResource(R.drawable.ic_heart_fillout);
            ih3.setImageResource(R.drawable.ic_heart_fillout);
            ih4.setImageResource(R.drawable.ic_heart_fillout);
        }
        if (lifespan == 2) {
            ih1.setImageResource(R.drawable.ic_heart_fill);
            ih2.setImageResource(R.drawable.ic_heart_fill);
            ih3.setImageResource(R.drawable.ic_heart_fillout);
            ih4.setImageResource(R.drawable.ic_heart_fillout);
        }
        if (lifespan == 3) {
            ih1.setImageResource(R.drawable.ic_heart_fill);
            ih2.setImageResource(R.drawable.ic_heart_fill);
            ih3.setImageResource(R.drawable.ic_heart_fill);
            ih4.setImageResource(R.drawable.ic_heart_fillout);
        }
        if (lifespan == 4) {
            ih1.setImageResource(R.drawable.ic_heart_fill);
            ih2.setImageResource(R.drawable.ic_heart_fill);
            ih3.setImageResource(R.drawable.ic_heart_fill);
            ih4.setImageResource(R.drawable.ic_heart_fill);
        }
        if (lifespan > 4) {
            lifeCount = lifeCount - 1;
            AlertDialog.Builder excess = new AlertDialog.Builder(ctx);
            excess.setMessage("Life Excess!");
            excess.setCancelable(true);
            final AlertDialog dialog = excess.create();
            dialog.show();
            final Timer dialogt = new Timer();
            dialogt.schedule(new TimerTask() {
                public void run() {
                    dialog.dismiss();
                    dialogt.cancel();
                }
            }, 1000);
        }

    }

    static void scoreUpdate(int points, int level) {
        int temp = 1 + 5 * timerECounter;
        userScore = userScore + temp;

        if (level == 1) {
            userChecked++;
            checkView.setText(String.valueOf(userChecked));
            SDEasyActivity.scoreValueView.setText(String.valueOf(userScore));
        } else if (level == 2) {
            SDMediateActivity.scoreValueView.setText(String.valueOf(userScore));
        } else if (level == 3) {
            SDFinalActivity.scoreValueView.setText(String.valueOf(userScore));
        }
    }

    private static void gameOver(final Context ctx, final int levelstat) {
        AlertDialog.Builder game_over = new AlertDialog.Builder(ctx);
        LayoutInflater gameOver_inflater = LayoutInflater.from(ctx);
        View overView = gameOver_inflater.inflate(R.layout.game_over, null);
        game_over.setView(overView);
        TextView points_total = (TextView) overView.findViewById(R.id.p_n_total);
        TextView level = (TextView) overView.findViewById(R.id.p_n_l);
        Button scoreBoard = (Button) overView.findViewById(R.id.BaG);
        final Button playAgain = (Button) overView.findViewById(R.id.BbG);
        final Button quitGame = (Button) overView.findViewById(R.id.BcG);
        final AlertDialog gameOver_inflater_Dialog = game_over.create();
        String x = "";
        if (levelstat == 1) {
            x = "Easy Round";
        } else if (levelstat == 2) {
            x = "Mediate Round";
        } else if (levelstat == 3) {
            x = "Final Round";
        }
        level.setText(x);
        points_total.setText(String.valueOf(userScore) + " points");
        scoreBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctx.startActivity(new Intent(ctx.getApplicationContext(), SDScoreActivity.class));
            }
        });
        playAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameOver_inflater_Dialog.dismiss();
                playAgain(levelstat);
            }
        });
        quitGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameOver_inflater_Dialog.dismiss();
                quit(levelstat, ctx);
            }
        });

        gameOver_inflater_Dialog.setCancelable(false);
        gameOver_inflater_Dialog.setCanceledOnTouchOutside(false);
        gameOver_inflater_Dialog.show();
    }

    static void gameNotify(final Context ctx, final int levelstat) {
        AlertDialog.Builder game_notify = new AlertDialog.Builder(ctx);
        LayoutInflater gameNotify_inflater = LayoutInflater.from(ctx);
        View overView = gameNotify_inflater.inflate(R.layout.game_notify, null);
        game_notify.setView(overView);
        TextView points_total = (TextView) overView.findViewById(R.id.p_n_total);
        TextView level = (TextView) overView.findViewById(R.id.p_n_l);
        Button scoreBoard = (Button) overView.findViewById(R.id.BaG);
        final Button playAgain = (Button) overView.findViewById(R.id.BbG);
        Button quitGame = (Button) overView.findViewById(R.id.BcG);

        final AlertDialog gameNotify_inflater_Dialog = game_notify.create();
        level.setText("Easy Round");
        points_total.setText(String.valueOf(userScore) + " points");
        scoreBoard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ctx.startActivity(new Intent(ctx.getApplicationContext(), SDScoreActivity.class));
            }
        });
        playAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameNotify_inflater_Dialog.dismiss();
                playAgain(levelstat);
            }
        });
        quitGame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gameNotify_inflater_Dialog.dismiss();
                quit(levelstat, ctx);
            }
        });

        gameNotify_inflater_Dialog.setCancelable(false);
        gameNotify_inflater_Dialog.setCanceledOnTouchOutside(false);
        gameNotify_inflater_Dialog.show();

    }

    static void gamePaused(final Context ctx, final int levelstat) {
        final AlertDialog.Builder pause_alert = new AlertDialog.Builder(ctx);
        LayoutInflater inflater = LayoutInflater.from(ctx);
        View pauseView = inflater.inflate(R.layout.pause_layout, null);
        pause_alert.setView(pauseView);

        Button resume = (Button) pauseView.findViewById(R.id.resume_i);
        final Button restart = (Button) pauseView.findViewById(R.id.restart_i);
        final Button quit = (Button) pauseView.findViewById(R.id.quit_i);

        final AlertDialog alertDialog = pause_alert.create();

        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                playAgain(levelstat);
            }
        });
        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                resume(levelstat);
            }
        });
        quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
                quit(levelstat, ctx);
            }
        });
        alertDialog.setCancelable(false);
        alertDialog.setCanceledOnTouchOutside(false);
        alertDialog.show();
    }

    private static void playAgain(int levelstat) {
        problemNumber = 0;
        userScore = 0;
        lifeCount = 4;
        if (levelstat == 1) {
            SDEasyActivity.getInstance().recreate();
        } else if (levelstat == 2) {
            SDMediateActivity.getInstance().recreate();
        } else if (levelstat == 3) {
            SDFinalActivity.getInstance().recreate();
        }
    }

    private static void quit(int levelstat, Context ctx) {
        gameStateSave(ctx, levelstat);
        problemNumber = 0;
        userScore = 0;
        if (levelstat == 1) {
            SDEasyActivity.getInstance().quit();
        } else if (levelstat == 2) {
            SDMediateActivity.getInstance().quit();
        } else if (levelstat == 3) {
            SDFinalActivity.getInstance().quit();
        }
    }

    private static void resume(int levelstat) {
        if (levelstat == 1) {
            SDEasyActivity.getInstance().resume();
        } else if (levelstat == 2) {
            SDMediateActivity.getInstance().resume();
        } else if (levelstat == 3) {
            SDFinalActivity.getInstance().resume();
        }
    }

    private static void gameStateSave(Context ctx, int level) {
        SharedPreferences gss = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor edit = gss.edit();
        edit.putString(gameState[level][0], String.valueOf(true));
        edit.putString(gameState[level][1], String.valueOf(userScore));
        edit.putString(gameState[level][2], String.valueOf(lifeCount));
        edit.putString(gameState[level][3], String.valueOf(problemNumber));
        edit.putString(gameState[level][4], String.valueOf(gameTimeState[level - 1]));
        edit.apply();
    }

    static void isLevelFinished(int level, Context ctx) {
        SharedPreferences isf = PreferenceManager.getDefaultSharedPreferences(ctx);
        SharedPreferences.Editor edit = isf.edit();
        edit.putBoolean(levelKeys[level - 1], true);
        edit.apply();
    }

    static String gameStateRetrieve(int level, String dataName, Context ctx) {
        String temp = "0";
        SharedPreferences dataRetrieve = PreferenceManager.getDefaultSharedPreferences(ctx);
        if (dataName == gameState[level][0]) {
            temp = dataRetrieve.getString((gameState[level][0]), "");
        } else if (dataName == gameState[level][1]) {
            temp = dataRetrieve.getString((gameState[level][1]), "");
        } else if (dataName == gameState[level][2]) {
            temp = dataRetrieve.getString((gameState[level][2]), "");
        } else if (dataName == gameState[level][3]) {
            temp = dataRetrieve.getString((gameState[level][3]), "");
        }
        return temp;
    }
}
