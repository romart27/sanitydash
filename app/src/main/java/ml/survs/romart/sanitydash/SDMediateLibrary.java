package ml.survs.romart.sanitydash;

/**
 * Created by romart on 5/22/17.
 * Kali GNU/Linux Rolling 64-bit
 */

class SDMediateLibrary {

    private String[] Problem = {
            "Tanya is older than Eric.\n" +
                    "Cliff is older than Tanya.\n" +
                    "Eric is older than Cliff.\n" +
                    " \n" +
                    "If the first two statements are true, the third statement is",
            "Blueberries cost more than strawberries.\n" +
                    "Blueberries cost less than raspberries.\n" +
                    "Raspberries cost more than both strawberries and blueberries.\n" +
                    " \n" +
                    "If the first two statements are true, the third statement is",
            "Mara runs faster than Gail.\n" +
                    "Lily runs faster than Mara.\n" +
                    "Gail runs faster than Lily.\n" +
                    " \n" +
                    "If the first two statements are true, the third statement is",
            "2, 1, (1/2), (1/4), ... What number should come next?",
            "7, 10, 8, 11, 9, 12, ... What number should come next?",
            "3 pumps, working 8 hours a day, can empty a tank in 2 days. How many hours a day must 4 pumps work to empty the tank in 1 day?",
            "All the offices on the 9th floor have wall-to-wall carpeting.\n" +
                    "No wall-to-wall carpeting is pink.\n" +
                    "None of the offices on the 9th floor has pink wall-to-wall carpeting.\n" +
                    " \n" +
                    "If the first two statements are true, the third statement is\n",
            "gorblflur means fan belt\n" +
                    "pixngorbl means ceiling fan\n" +
                    "arthtusl means tile roof\n" +
                    " \n" +
                    "Which word could mean \"ceiling tile\"?",
            "Here are some words translated from an artificial language.\n" +
                    " \n" +
                    "hapllesh means cloudburst\n" +
                    "srenchoch means pinball\n" +
                    "resbosrench means ninepin\n" +
                    " \n" +
                    "Which word could mean \"cloud nine\"?",
            "Here are some words translated from an artificial language.\n" +
                    " \n" +
                    "agnoscrenia means poisonous spider\n" +
                    "delanocrenia means poisonous snake\n" +
                    "agnosdeery means brown spider\n" +
                    " \n" +
                    "Which word could mean \"black widow spider\"?",
            "Choose the word which is different from the rest.",
            "Choose the word which is different from the rest.",
            "Choose the word which is different from the rest.",
            "A sum fetched a total simple interest of 4016.25 at the rate of 9 %.p.a. in 5 years. What is the sum?",
            "SCD, TEF, UGH, ____, WKL",
            "B2CD, _____, BCD4, B5CD, BC6D",
            "FAG, GAF, HAI, IAH, ____",
            "ELFA, GLHA, ILJA, _____, MLNA",
            "Here are some words translated from an artificial language.\n" +
                    " \n" +
                    "godabim means kidney stones\n" +
                    "romzbim means kidney beans\n" +
                    "romzbako means wax beans\n" +
                    " \n" +
                    "Which word could mean \"wax statue\"?",
            "Here are some words translated from an artificial language.\n" +
                    " \n" +
                    "granamelke means big tree\n" +
                    "pinimelke means little tree\n" +
                    "melkehoon means tree house\n" +
                    " \n" +
                    "Which word could mean \"big house\"?"
    };

    private String[] Answers = {
            "Third statement must be false",
            "Yes, its True",
            "Its True",
            "(1/8)",
            "10",
            "12",
            "Its True",
            "pixnarth",
            "haplresbo",
            "agnosvitriblunin",
            "Eagle",
            "Oil",
            "Potassium",
            "8925",
            "VIJ",
            "BC3D",
            "JAK",
            "KLLA",
            "wasibako",
            "granahoon"
    };

    private String[][] Choices = {
            {"Third statement must be false", "Might be true", "Certainly", "Nothing"},
            {"Certainly", "Yes, its True", "Nope", "I don't know"},
            {"False", "Not True", "Its True", "Maybe"},
            {"(1/3)", "(1/8)", "(2/8)", "(1/16)"},
            {"10", "7", "12", "13"},
            {"12", "9", "11", "10"},
            {"Its True", "Its not True", "Certainly", "Maybe"},
            {"gorbltusl", "pixnarth", "flurgorbl", "arthflur"},
            {"leshsrench", "ochhapl", "haploch", "haplresbo"},
            {"deeryclostagnos", "agnosdelano", "agnosvitriblunin", "trymuttiagnos"},
            {"Kiwi", "Eagle", "Emu", "Ostrich"},
            {"Cream", "Oil", "Butter", "Cheese"},
            {"Potassium", "Silicon", "Zirconium", "Gallium"},
            {"4462.50", "8032.50", "8925", "None of these"},
            {"CMN", "UJI", "VIJ", "IJT"},
            {"B2C2D", "BC3D", "B2C3D", "BCD7"},
            {"JAK", "HAL", "HAK", "JAI"},
            {"OLPA", "KLMA", "LLMA", "KLLA"},
            {"godaromz", "lazbim", "wasibako", "romzpeo"},
            {"granahoon", "pinishur", "pinihoon", "melkegrana"},
    };
    private String[] Hint = {
            "Because the first two statements are true, Eric is the youngest of the three.",
            "Raspberries are the most expensive of the three.",
            "Lily runs the fastest",
            "Look for the degree and direction of change between the numbers. In other words, do the numbers increase or decrease, and by how much",
            "A simple alternating addition and subtraction series",
            "Let the required number of working hours per day to be x",
            "No wall-to-wall carpeting is pink and all the offices have wall-to-wall carpeting",
            "The question that follow will ask you to reverse the process and translate an English word into the artificial language.\n",
            "Hmmm..",
            "Noun appears first and the adjectives follow.",
            "Flightless",
            "Hmm..",
            "Semiconductor devices",
            "Principal = (100 x 4016.25)/(9 x 5)",
            "There are two alphabetical series here.",
            "simple 2, 3, 4, 5, 6 series",
            "first letters are in alphabetical order: F, G, H, I , J.",
            "The second and forth letters in the series, L and A, are static.",
            "Hmm..",
            "Hmm.."
    };

    String getHint(int a) {
        String hint;
        if (Hint.length > a) {
            hint = Hint[a];
        } else {
            hint = Hint[1];
        }
        return hint;
    }

    String getProblem(int a) {
        String problem;
        if (Problem.length > a) {
            problem = Problem[a];
        } else {
            problem = Problem[1];
        }
        return problem;
    }

    String getAnswers(int a) {
        String answers;
        if (Answers.length > a) {
            answers = Answers[a];
        } else {
            answers = Answers[1];
        }
        return answers;
    }

    String choiceA(int a, int b) {
        return Choices[a][b];
    }

    String choiceB(int a, int b) {
        return Choices[a][b];
    }

    String choiceC(int a, int b) {
        return Choices[a][b];
    }

    String choiceD(int a, int b) {
        return Choices[a][b];
    }

}
