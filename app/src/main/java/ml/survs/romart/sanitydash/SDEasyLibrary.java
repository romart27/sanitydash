package ml.survs.romart.sanitydash;

/**
 * Created by romart on 5/21/17.
 * Kali GNU/Linux Rolling 64-bit
 */

class SDEasyLibrary {

    //Image Resources for problems (29 Problems)
    private Object[] problems = {
            R.drawable.a1q,
            R.drawable.les1q,
            R.drawable.les2q,
            R.drawable.les3q,
            R.drawable.les4q,
            R.drawable.les5q,
            R.drawable.les6q,
            R.drawable.les7q,
            R.drawable.les8q,
            R.drawable.les9q,
            R.drawable.les10q,
            R.drawable.mir1q,
            R.drawable.mir2q,
            R.drawable.mir3q,
            R.drawable.mir4q,
            R.drawable.mir5q,
            R.drawable.mir6q,
            R.drawable.mir7q,
            R.drawable.mir8q,
            R.drawable.mir9q,
            R.drawable.mir10q,
            R.drawable.mir11q,
            R.drawable.mir12q,
            R.drawable.mir13q,
            R.drawable.mir14q,
            R.drawable.mir15q,
            R.drawable.riq1,
            R.drawable.riq2q,
            R.drawable.riq3q,
    };
    //Problem Corresponding Choices 2D Array Implementation
    private Object[][] choices = {
            {R.drawable.a1a, R.drawable.a1b, R.drawable.a1c, R.drawable.a1d},
            {R.drawable.les1a, R.drawable.l1, R.drawable.les1c, R.drawable.les1d},
            {R.drawable.les2a, R.drawable.les2b, R.drawable.les2c, R.drawable.les2d},
            {R.drawable.les3a, R.drawable.les3b, R.drawable.les3c, R.drawable.les3d},
            {R.drawable.les4a, R.drawable.les4b, R.drawable.les4c, R.drawable.les4d},
            {R.drawable.les5a, R.drawable.les5b, R.drawable.les5c, R.drawable.les5d},
            {R.drawable.les6a, R.drawable.les6b, R.drawable.les6c, R.drawable.les6d},
            {R.drawable.les7a, R.drawable.les7b, R.drawable.les7c, R.drawable.les7d},
            {R.drawable.les8a, R.drawable.les8b, R.drawable.les8c, R.drawable.les8d},
            {R.drawable.les9a, R.drawable.les9b, R.drawable.les9c, R.drawable.les9d},
            {R.drawable.les10a, R.drawable.les10b, R.drawable.les10c, R.drawable.les10d},
            {R.drawable.mir1a, R.drawable.mir1b, R.drawable.mir1c, R.drawable.mir1d},
            {R.drawable.mir2a, R.drawable.mir2b, R.drawable.mir2c, R.drawable.mir2d},
            {R.drawable.mir3a, R.drawable.mir3b, R.drawable.mir3c, R.drawable.mir3d},
            {R.drawable.mir4a, R.drawable.mir4b, R.drawable.mir4c, R.drawable.mir4d},
            {R.drawable.mir5a, R.drawable.mir5b, R.drawable.mir5c, R.drawable.mir5d},
            {R.drawable.mir6a, R.drawable.mir6b, R.drawable.mir6c, R.drawable.mir6d},
            {R.drawable.mir7a, R.drawable.mir7b, R.drawable.mir7c, R.drawable.mir7d},
            {R.drawable.mir8a, R.drawable.mir8b, R.drawable.mir8c, R.drawable.mir8d},
            {R.drawable.mir9a, R.drawable.mir9b, R.drawable.mir9c, R.drawable.mir9d},
            {R.drawable.mir10a, R.drawable.mir10b, R.drawable.mir10c, R.drawable.mir10d},
            {R.drawable.mir11a, R.drawable.mir11b, R.drawable.mir11c, R.drawable.mir11d},
            {R.drawable.mir12a, R.drawable.mir12b, R.drawable.mir12c, R.drawable.mir12d},
            {R.drawable.mir13a, R.drawable.mir13b, R.drawable.mir13c, R.drawable.mir13d},
            {R.drawable.mir14a, R.drawable.mir14b, R.drawable.mir14c, R.drawable.mir14d},
            {R.drawable.mir15c, R.drawable.mir15b, R.drawable.mir15c, R.drawable.mir15d},
            {R.drawable.riq1a, R.drawable.riq1b, R.drawable.riq1c, R.drawable.riq1d},
            {R.drawable.riq2a


                    , R.drawable.riq2b, R.drawable.riq2c, R.drawable.riq2d},
            {R.drawable.riq3a, R.drawable.riq3b, R.drawable.riq3c, R.drawable.riq3d},
    };


    //Problem Answer Keys
    private Object answers[] = {
            R.drawable.a1a,
            R.drawable.les1c,
            R.drawable.les2b,
            R.drawable.les3a,
            R.drawable.les4d,
            R.drawable.les5a,
            R.drawable.les6d,
            R.drawable.les7c,
            R.drawable.les8c,
            R.drawable.les9d,
            R.drawable.les10b,
            R.drawable.mir1b,
            R.drawable.mir2d,
            R.drawable.mir3d,
            R.drawable.mir4b,
            R.drawable.mir5a,
            R.drawable.mir6d,
            R.drawable.mir7b,
            R.drawable.mir8a,
            R.drawable.mir9d,
            R.drawable.mir10c,
            R.drawable.mir11d,
            R.drawable.mir12a,
            R.drawable.mir13a,
            R.drawable.mir14b,
            R.drawable.mir15b,
            R.drawable.riq1a,
            R.drawable.riq2a,
            R.drawable.riq3a,
    };

    private String mirrorTypeHint = "In each of the following questions you are given a combination of alphabets and/or numbers followed by four alternatives (1), (2), (3) and (4). Choose the alternative which closely resembles the mirror image of the given combination.";
    private String diagrammaticType = "For each of the following diagrams, select the item below which would complete the pattern";

    //Problem Hints
    private String Hint[] = {
            diagrammaticType,
            diagrammaticType,
            diagrammaticType,
            diagrammaticType,
            diagrammaticType,
            diagrammaticType,
            diagrammaticType,
            diagrammaticType,
            diagrammaticType,
            diagrammaticType,
            diagrammaticType,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            mirrorTypeHint,
            diagrammaticType,
            diagrammaticType,
            diagrammaticType,
    };

    String getHint(int a) {
        String hint;
        if (Hint.length > a) {
            hint = Hint[a];
        } else {
            hint = Hint[1];
        }
        return hint;
    }

    Object getProblem(int a) {
        Object problem;
        if (problems.length > a) {
            problem = problems[a];
        } else {
            problem = problems[1];
        }
        return problem;
    }

    Object getAnswer(int a) {
        Object answer;
        if (answers.length > a) {
            answer = answers[a];
        } else {
            answer = answers[1];
        }
        return answer;
    }

    Object choiceA(int a, int b) {
        return choices[a][b];
    }

    Object choiceB(int a, int b) {
        return choices[a][b];
    }

    Object choiceC(int a, int b) {
        return choices[a][b];
    }

    Object choiceD(int a, int b) {
        return choices[a][b];
    }

}
