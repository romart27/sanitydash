package ml.survs.romart.sanitydash;

/**
 * Created by romart on 5/23/17.
 * Kali GNU/Linux Rolling 64-bit
 */

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class SDScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        FloatingActionButton favBack = (FloatingActionButton) findViewById(R.id.favBack);
        favBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SDScoreActivity.super.finish();
                SDScoreActivity.super.onBackPressed();
            }
        });
    }
}
