package ml.survs.romart.sanitydash;

/*
  Created by romart on 5/24/17.
  Kali GNU/Linux Rolling 64-bit
*/

import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;
import java.text.NumberFormat;

import static ml.survs.romart.sanitydash.SDFramework.gamePaused;
import static ml.survs.romart.sanitydash.SDFramework.ih1;
import static ml.survs.romart.sanitydash.SDFramework.ih2;
import static ml.survs.romart.sanitydash.SDFramework.ih3;
import static ml.survs.romart.sanitydash.SDFramework.ih4;
import static ml.survs.romart.sanitydash.SDFramework.lifeCount;
import static ml.survs.romart.sanitydash.SDFramework.lifeSpan;
import static ml.survs.romart.sanitydash.SDFramework.onCheck;
import static ml.survs.romart.sanitydash.SDFramework.onHint;
import static ml.survs.romart.sanitydash.SDFramework.onWrong;
import static ml.survs.romart.sanitydash.SDFramework.problemNumber;
import static ml.survs.romart.sanitydash.SDFramework.scoreUpdate;
import static ml.survs.romart.sanitydash.SDFramework.userScore;

public class SDFinalActivity extends AppCompatActivity {

    public static TextView scoreValueView;
    public static int timerFCounter = 0;
    static SDFinalActivity sdFinalActivity;
    private SDFinalLibrary questionLibrary = new SDFinalLibrary();
    private TextView questionNumberView;
    private TextView timerF;
    private String userAnswer;
    private String choiceAs;
    private String choiceBs;
    private String choiceCs;
    private String choiceDs;
    private String choiceEs;
    private String choiceFs;
    private String choiceGs;
    private Button choicesA;
    private Button choicesB;
    private Button choicesC;
    private Button choicesD;
    private Button choicesE;
    private Button choicesF;
    private Button choicesG;
    private int levelIdentity = 3;

    private boolean ver1 = false;

    private SDFinalActivity.MyFinalTimer myFinalTimer;

    private ProgressBar progressBar;

    private String Hint = "";

    private Context ctx = SDFinalActivity.this;

    public static SDFinalActivity getInstance() {
        return sdFinalActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level_two);
        lifeSpanInitializer();
        sdFinalActivity = this;

        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        scoreValueView = (TextView) findViewById(R.id.scoreValue);
        questionNumberView = (TextView) findViewById(R.id.questionNumber);
        timerF = (TextView) findViewById(R.id.timerF);

        Button favHint = (Button) findViewById(R.id.favHint);

        choicesA = (Button) findViewById(R.id.choiceA);

        favHint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onHint(ctx, Hint);
            }
        });

        choicesA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceAs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesB = (Button) findViewById(R.id.choiceB);
        choicesB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceBs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesC = (Button) findViewById(R.id.choiceC);
        choicesC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceCs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesD = (Button) findViewById(R.id.choiceD);
        choicesD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceDs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesE = (Button) findViewById(R.id.choiceE);
        choicesE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceEs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesF = (Button) findViewById(R.id.choiceF);
        choicesF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceFs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });

        choicesG = (Button) findViewById(R.id.choiceG);
        choicesG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (userAnswer.equals(choiceGs)) {
                    userScore = userScore + 1;
                    scoreUpdate(userScore, levelIdentity);
                    onCheck(ctx);
                } else {
                    onWrong(ctx, true, levelIdentity);
                }
                refreshProblem();
            }
        });
        lifeSpan(lifeCount, ctx, levelIdentity);
        refreshProblem();
    }

    public void lifeSpanInitializer() {
        ih1 = (ImageView) findViewById(R.id.ih1);
        ih2 = (ImageView) findViewById(R.id.ih2);
        ih3 = (ImageView) findViewById(R.id.ih3);
        ih4 = (ImageView) findViewById(R.id.ih4);
    }

    private void refreshProblem() {
        if (lifeCount < 1) {
            myFinalTimer.cancel();
        } else {
            if (ver1) {
                myFinalTimer.cancel();
            } else {
                ver1 = true;
            }
            timerStart(true, 0);
        }
        if (problemNumber < 1) {
            NumberFormat valueDecimal = new DecimalFormat("0");
            int[][] cTemp = {
                    {1, 2, 3, 0, 4, 6, 5},
                    {2, 1, 3, 0, 5, 4, 6},
                    {5, 3, 2, 6, 0, 1, 4},
                    {1, 0, 4, 6, 5, 3, 2}
            };
            //Randomize Problem Choices
            int x = Integer.parseInt(valueDecimal.format(Math.floor(Math.random() * 6)));
            //Randomize Problem from 0 to 29
            int temp = Integer.parseInt(valueDecimal.format(Math.floor(Math.random() * 3)));
            //Setting up for problem Question Image
            ImageView imageContent = (ImageView) findViewById(R.id.imageContent);
            imageContent.setImageResource((Integer) questionLibrary.getProblem(temp));

            choicesA.setText(questionLibrary.choiceA(temp, cTemp[x][0]));
            choicesB.setText(questionLibrary.choiceB(temp, cTemp[x][1]));
            choicesC.setText(questionLibrary.choiceC(temp, cTemp[x][2]));
            choicesD.setText(questionLibrary.choiceD(temp, cTemp[x][3]));
            choicesE.setText(questionLibrary.choiceE(temp, cTemp[x][4]));
            choicesF.setText(questionLibrary.choiceF(temp, cTemp[x][5]));
            choicesG.setText(questionLibrary.choiceG(temp, cTemp[x][6]));
            userAnswer = questionLibrary.getAnswers(temp);
            Hint = questionLibrary.getHint(temp);
            choiceAs = questionLibrary.choiceA(temp, cTemp[x][0]);
            choiceBs = questionLibrary.choiceB(temp, cTemp[x][1]);
            choiceCs = questionLibrary.choiceC(temp, cTemp[x][2]);
            choiceDs = questionLibrary.choiceD(temp, cTemp[x][3]);
            choiceEs = questionLibrary.choiceE(temp, cTemp[x][4]);
            choiceFs = questionLibrary.choiceF(temp, cTemp[x][5]);
            choiceGs = questionLibrary.choiceG(temp, cTemp[x][6]);
            problemNumber++;
            questionNumberView.setText(getString(R.string.lvl_one_15_counter) + String.valueOf(problemNumber));
        } else {
            //////////

        }
    }

    private void timerStart(boolean ver2, int modValue) {
        if (ver2) {
            myFinalTimer = new SDFinalActivity.MyFinalTimer(61000, 500);
            myFinalTimer.start();
        } else {
            myFinalTimer = new SDFinalActivity.MyFinalTimer(modValue * 1000, 500);
            myFinalTimer.start();
        }
    }

    @Override
    public void onBackPressed() {
        myFinalTimer.cancel();
        gamePaused(ctx, levelIdentity);
    }

    public void quit() {
        super.onBackPressed();
    }

    public void resume() {
        myFinalTimer = new SDFinalActivity.MyFinalTimer((timerFCounter * 1000), 500);
        myFinalTimer.start();
    }

    private class MyFinalTimer extends CountDownTimer {

        MyFinalTimer(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        @Override
        public void onTick(long millisUntilFinished) {
            timerFCounter = Integer.parseInt(String.valueOf(millisUntilFinished / 1000));
            timerF.setText(String.valueOf(timerFCounter) + "s");
            progressBar.setProgress(timerFCounter);
        }

        @Override
        public void onFinish() {
            refreshProblem();
            onWrong(ctx, false, levelIdentity);
            myFinalTimer.cancel();
            Toast.makeText(SDFinalActivity.this, "Times Up", Toast.LENGTH_SHORT).show();
            if (lifeCount < 1) {
                myFinalTimer.cancel();
            } else {
                timerStart(true, 0);
            }
        }

    }
}


