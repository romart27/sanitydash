package ml.survs.romart.sanitydash;

/**
 * Created by romart on 5/24/17.
 * Kali GNU/Linux Rolling 64-bit
 */

class SDFinalLibrary {

    //4 Problems for Difficult Level
    private Object[] Problem = {
            R.drawable.q1,
            R.drawable.q2,
            R.drawable.q3,
            R.drawable.q5,
    };

    private String[] Answers = {
            "A",
            "D",
            "E",
            "F",
    };

    private String[][] Choices = {
            {"A", "B", "C", "D", "E", "F", "G"},
            {"A", "B", "C", "D", "E", "F", "G"},
            {"A", "B", "C", "D", "E", "F", "G"},
            {"A", "B", "C", "D", "E", "F", "G"},
    };

    private String[] Hint = {
            "No more Hints",
            "No more Hints",
            "No more Hints",
            "No more Hints",
    };

    String getHint(int a) {
        String hint;
        if (Hint.length > a) {
            hint = Hint[a];
        } else {
            hint = Hint[1];
        }
        return hint;
    }

    Object getProblem(int a) {
        Object problem;
        if (Problem.length > a) {
            problem = Problem[a];
        } else {
            problem = Problem[1];
        }
        return problem;
    }

    String getAnswers(int a) {
        String answers;
        if (Answers.length > a) {
            answers = Answers[a];
        } else {
            answers = Answers[1];
        }
        return answers;
    }

    String choiceA(int a, int b) {
        return Choices[a][b];
    }

    String choiceB(int a, int b) {
        return Choices[a][b];
    }

    String choiceC(int a, int b) {
        return Choices[a][b];
    }

    String choiceD(int a, int b) {
        return Choices[a][b];
    }

    String choiceE(int a, int b) {
        return Choices[a][b];
    }

    String choiceF(int a, int b) {
        return Choices[a][b];
    }

    String choiceG(int a, int b) {
        return Choices[a][b];
    }
}
